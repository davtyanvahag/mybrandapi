var jwt = require('jsonwebtoken');
var Admins = require('../models/Admins');
var moment = require('moment');


//function for auth checking trow token
//token can be sent by post body, url params and by headers
//used
exports.checkAdminToken = function (req, callback) {
    var token = req.body.token || req.headers.token || req.params.token;
    if (typeof token != 'undefined' && token != null && token != '') {
        jwt.verify(token, global.secret, function (err, decoded) {
            if (!err && typeof decoded != 'undefined') {
                Admins.findOne({username: decoded.data.username}, function (err, user) {
                    if (!err && user != null && typeof  user != "undefined") {
                        callback(false, user)
                    } else {
                        callback(true, {});
                    }
                });
            } else {
                callback(true, {});
            }
        });
    } else {
        callback(true, {})
    }
};



