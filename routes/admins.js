var express = require('express');
var router = express.Router();
var AdminController = require('../controllers/AdminController');


//for firs admin only
router.post('/registration', function (req, res) {
    AdminController.registration(req, res);
});

router.post('/login', function (req, res) {
    AdminController.logIn(req, res);
});

router.post('/addAdmin', function (req, res) {
    AdminController.addAdmin(req, res);
});

router.put('/updateAccount', function (req, res) {
    AdminController.updateAccount(req, res);
});

router.put('/updatePassword', function (req, res) {
  AdminController.updatePassword(req, res);
});

router.delete('/deleteAdmin/:id', function (req, res) {
    AdminController.deleteAdmin(req, res);
});

router.get('/getAdminsList', function (req, res) {
    AdminController.getAdminsList(req, res);
});

router.get('/getCurrentAdmin', function (req, res) {
    AdminController.getCurrentAdmin(req, res);
});

router.get('/getAdmin/:id', function (req, res) {
  AdminController.getAdmin(req, res);
});


module.exports = router;