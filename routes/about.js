var express = require('express');
var router = express.Router();
var AboutController = require('../controllers/AboutController');

router.get('/get', function (req, res) {
  AboutController.get(req, res);
});

router.post('/add', function (req, res) {
  AboutController.add(req, res);
});



module.exports = router;