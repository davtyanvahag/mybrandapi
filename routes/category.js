var express = require('express');
var router = express.Router();
var CategoryController = require('../controllers/CategoryController');

router.post('', function (req, res) {
    CategoryController.addCategory(req, res);
});

router.get('/getCategory/:id', function (req, res) {
    CategoryController.getCategory(req, res);
});

router.get('', function (req, res) {
    CategoryController.getAllCategories(req, res);
});

router.delete('/deleteCategory/:id', function (req, res) {
    CategoryController.deleteCategory(req, res);
});

router.put('/updateCategory/:id', function (req, res) {
    CategoryController.updateCategory(req, res);
});

module.exports = router;