var express = require('express');
var router = express.Router();
var CreateByController = require('../controllers/CreateByController');

router.post('', (req, res)  => {
  CreateByController.add(req, res);
});

router.get('', (req, res) => {
  CreateByController.get(req, res);
});

module.exports = router;