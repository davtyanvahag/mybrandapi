var express = require('express');
var router = express.Router();
var ContactsController = require('../controllers/ContactsController');

router.get('/get', function (req, res) {
  ContactsController.getAllContacts(req, res);
});

router.post('/add', function (req, res) {
    ContactsController.addContact(req, res);
});



module.exports = router;