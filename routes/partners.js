var express = require('express');
var router = express.Router();
var PartnersController = require('../controllers/PartnersController');

router.post('', function (req, res) {
    PartnersController.addPartner(req, res);
});

router.get('/:id/:page/:limit', function (req, res) {
    PartnersController.getAllPartners(req, res);
});

router.get('/all', function (req, res) {
  PartnersController.getAll(req, res);
});

router.delete('/deletePartner/:id', function (req, res) {
    PartnersController.deletePartner(req, res);
});

module.exports = router;