var express = require('express');
var router = express.Router();
var HomeController = require('../controllers/HomeController');

router.post('/add', (req, res) => {
    HomeController.add(req, res);
});
router.get('/getAll', (req, res) => {
  HomeController.getAll(req, res);
});

router.get('/get/:id', (req, res) => {
    HomeController.get(req, res);
});


router.post('/uploadProductImg/:token',  (req, res) => {
  HomeController.uploadImage(req, res);
})

router.delete('/delete/:id', (req, res) => {
    HomeController.delete(req, res);
});

router.put('/update', (req, res) => {
    HomeController.update(req, res);
});

router.post('/uploadImage/:token', function(req, res, next) {
  HomeController.uploadImage(req, res);
});

router.post('/uploadImageEdit/:token/:id', function(req, res, next) {
  HomeController.uploadImageEdit(req, res);
});


router.post('/updateAndDeleteImage', function(req, res, next) {
  HomeController.updateAndDeleteImage(req, res);
});




module.exports = router;