var express = require('express');
var router = express.Router();
var ItemController = require('../controllers/ItemsController');

router.post('/add', (req, res) => {
  ItemController.add(req, res);
});
router.get('/getAll', (req, res) => {
  ItemController.getAll(req, res);
});

router.get('/:id/:page/:limit', function (req, res) {
  ItemController.getAllLimitedItems(req, res);
});

router.get('/get/:id', (req, res) => {
  ItemController.get(req, res);
});


router.post('/uploadProductImg/:token',  (req, res) => {
  ItemController.uploadImage(req, res);
})

router.delete('/delete/:id', (req, res) => {
  ItemController.delete(req, res);
});

router.put('/update', (req, res) => {
  ItemController.update(req, res);
});

router.post('/uploadImage/:token', function(req, res, next) {
  ItemController.uploadImage(req, res);
});

router.post('/uploadImageEdit/:token/:id', function(req, res, next) {
  ItemController.uploadImageEdit(req, res);
});


router.post('/updateAndDeleteImage', function(req, res, next) {
  ItemController.updateAndDeleteImage(req, res);
});




module.exports = router;