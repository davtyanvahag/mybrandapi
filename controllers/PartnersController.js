var Partners = require('../models/Partners');
var Admins = require('../models/Admins');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');

//create new Partner
//only admins
//tested
exports.addPartner = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['image', 'name'], function (err, errProps) {
                if (!err) {
                    var newPartner = new Partners({
                        name: req.body.name,
                        image: req.body.image
                    });
                    newPartner.save(function (err, data) {
                        if (!err) {
                            res.json({error: false, data: data});
                        } else {
                            res.json({error: true, message: "Error on adding new Partner!"});
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on creating new Partner"})
        }
    })
};

//delete Partner
//only admin
//tested
exports.deletePartner = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    Partners.findOne({_id: req.params.id}, function (err, data) {
                        if (!err && data != null) {
                            Partners.remove({_id: req.params.id}, function (err, data) {
                                if (!err) {
                                    res.json({
                                        error: false,
                                        message: "Deleted successfully"
                                    })
                                } else {
                                    res.json({
                                        error: true,
                                        message: "error on delete Partner"
                                    })
                                }
                            })
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on deleting Partner"})
        }
    })
};

//get one Partner by Id
//all
//tested
exports.getPartner = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    Partners.find({_id: req.params.id}, function (err, data) {
                        if (!err && data != null) {
                            res.json({error: false, data: data})
                        } else {
                            res.json({error: true, massage: "no partners in db"})
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on getting Partner"})
        }
    })
};


exports.getAll = function (req, res) {
    Partners.find({}, function(err, data) {
      if (err)
        res.json({"error":true});
      else
        res.json({
          "error":false,
          "data": data
        });
    });
};
//get all partners
//all
//tested
exports.getAllPartners = function (req, res) {
  if(req.params.id === 'all'){
    Partners.count({},function(err,count){
      Partners.find({}, null).skip(Number(req.params.page) > 0 ? ((Number(req.params.page) - 1) * Number(req.params.limit)) : 0)
        .limit(+req.params.limit).exec(function(err, data) {
        if (err)
          res.json({"error":true});
        else
          res.json({
            "total": count,
            "error":false,
            "data": data
          });
      });
    });
  }else{
    Partners.count({name:req.params.id},function(err,count){
      Partners.find({name:new RegExp(req.params.id)}, null).skip(Number(req.params.page) > 0 ? ((Number(req.params.page) - 1) * Number(req.params.limit)) : 0)
        .limit(+req.params.limit).exec(function(err, data) {
        if (err)
          res.json({"error":true});
        else
          res.json({
            "total": count,
            "error":false,
            "data": data
          });
      });
    });
  }
};

//update Partner
//only admins
//tested
exports.updatePartner = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['partner_id', 'img', 'name'], function (err, errProps) {
                if (!err) {
                    Partners.findOne({_id: req.body.partner_id}, function (err, part) {
                        if (!err && part != null && typeof part != "undefined") {
                            part.name = req.body.name;
                            part.img = req.body.img;

                            part.save(function (err, data) {
                                if (!err) {
                                    res.json({error: false, data: data});
                                } else {
                                    res.json({
                                        error: true,
                                        message: "Error on update Partner!!!"
                                    })
                                }
                            })
                        } else {
                            res.json({error: true, message: "Error on selecting Partner!!"})
                        }
                    });
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on updating Partner"})
        }
    })
};



