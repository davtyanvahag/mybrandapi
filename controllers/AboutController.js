var About = require('../models/About');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');

//create new Contact
//only admins
//tested
exports.add = function (req, res) {
  authHelper.checkAdminToken(req, function (err, data) {
    if (!err) {
      reqDataHelper.validateReqData(req, 'body', ['_id','descr', 'videolink'], function (err, errProps) {
        if (!err) {
          About.findOne({_id: req.body._id}, function (err, data) {
            if (!err) {
              About.update({_id: req.body._id},{
                descr: req.body.descr,
                videolink: req.body.videolink
              }, function (err, data) {
                if (!err) {
                  res.json({error: false, data: data});
                } else {
                  res.json({error: true, message: "Error on adding new Contact!"});
                }
              })
            } else {
              let about = new About({
                descr: req.body.descr,
                videolink: req.body.videolink
              });
              about.save(function (err, data) {
                if (!err) {
                  res.json({error: false, data: data});
                } else {
                  res.json({error: true, message: "Error on adding new Contact!"});
                }
              })
            }
          } )
        } else {
          res.json({error: true, message: "Validation error", errProps: errProps});
        }
      })
    } else {
      res.json({error: true, message: "Error on creating new Contact"})
    }
  })
};
//get all contacts
//all
//tested
exports.get = function (req, res) {
  reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
    if (!err) {
      About.find({}, function (err, data) {
        if (!err && data != null) {
          res.json({error: false, data: data})
        } else {
          res.json({error: true, massage: "no contacts in db!!"})
        }
      })
    } else {
      res.json({error: true, message: "Validation error", errProps: errProps});
    }
  })
};


