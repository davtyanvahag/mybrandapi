var Contacts = require('../models/Contacts');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');
var ObjectId = require('mongoose').Types.ObjectId;

//create new Contact
//only admins
//tested
exports.addContact = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['_id','emails', 'phones', 'fb', 'insta', 'linkedin', 'youtube', 'addresses'], function (err, errProps) {
                if (!err) {
                  Contacts.findOne({_id: req.body._id}, function (err, data) {
                    if (!err) {
                      Contacts.update({_id: req.body._id},{
                        emails: req.body.emails,
                        phones: req.body.phones,
                        fb: req.body.fb,
                        insta: req.body.insta,
                        linkedin: req.body.linkedin,
                        youtube: req.body.youtube,
                        addresses: req.body.addresses
                      }, function (err, data) {
                        if (!err) {
                          res.json({error: false, data: data});
                        } else {
                          res.json({error: true, message: "Error on adding new Contact!"});
                        }
                      })
                    } else {
                      let conacts = new Contacts({
                        emails: req.body.emails,
                        phones: req.body.phones,
                        fb: req.body.fb,
                        insta: req.body.insta,
                        linkedin: req.body.linkedin,
                        youtube: req.body.youtube,
                        addresses: req.body.addresses
                      });
                      conacts.save(function (err, data) {
                        if (!err) {
                          res.json({error: false, data: data});
                        } else {
                          res.json({error: true, message: "Error on adding new Contact!"});
                        }
                      })
                    }
                  } )
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on creating new Contact"})
        }
    })
};
//get all contacts
//all
//tested
exports.getAllContacts = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
        if (!err) {
            Contacts.find({}, function (err, data) {
                if (!err && data != null) {
                    res.json({error: false, data: data})
                } else {
                    res.json({error: true, massage: "no contacts in db!!"})
                }
            })
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    })
};


