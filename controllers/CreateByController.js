var CreateBy = require('../models/CreateBy');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');
var ObjectId = require('mongoose').Types.ObjectId;

exports.add = function (req, res) {
  authHelper.checkAdminToken(req, function (err, data) {
    if (!err) {
      reqDataHelper.validateReqData(req, 'body', ['_id','name','link', 'image'], function (err, errProps) {
        if (!err) {
          CreateBy.findOne({_id: req.body._id}, function (err, data) {
            if (!err) {
              CreateBy.update({_id: req.body._id},{
                name: req.body.name,
                link: req.body.link,
                image: req.body.image
              }, function (err, data) {
                if (!err) {
                  res.json({error: false, data: data});
                } else {
                  res.json({error: true, message: "Error on adding new Contact!"});
                }
              })
            } else {
              let createBy = new CreateBy({
                name: req.body.name,
                link: req.body.link,
                image: req.body.image
              });
              createBy.save(function (err, data) {
                if (!err) {
                  res.json({error: false, data: data});
                } else {
                  res.json({error: true, message: "Error on adding new Contact!"});
                }
              })
            }
          } )
        } else {
          res.json({error: true, message: "Validation error", errProps: errProps});
        }
      })
    } else {
      res.json({error: true, message: "Error on creating new Contact"})
    }
  })
};

exports.get = function (req, res) {
  reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
    if (!err) {
      CreateBy.find({}, function (err, data) {
        if (!err && data != null) {
          res.json({error: false, data: data})
        } else {
          req.json({error: true, massage: "no data in db"})
        }
      })
    } else {
      res.json({error: true, message: "Validation error", errProps: errProps});
    }
  })
};