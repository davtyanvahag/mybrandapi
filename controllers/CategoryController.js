var Category = require('../models/Category');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');

//create new Category
//only admins
//tested
exports.addCategory = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
        console.log(req.body)
            reqDataHelper.validateReqData(req, 'body', ['title', 'descr', 'image'], function (err, errProps) {
                if (!err) {
                    var newCategory = new Category({
                        title: req.body.title,
                        descr: req.body.descr,
                        image: req.body.image
                    });
                    newCategory.save(function (err, data) {
                        if (!err) {
                            res.json({error: false, data: data});
                        } else {
                            res.json({error: true, message: "Error on adding new category!"});
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on creating new Category"})
        }
    })
};

//delete Category
//only admins
//tested
exports.deleteCategory = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    Category.findOne({_id: req.params.id}, function (err, data) {
                        if (!err && data != null) {
                            Category.remove({_id: req.params.id}, function (err, data) {
                                if (!err) {
                                    res.json({
                                        error: false,
                                        message: "Deleted successfully"
                                    })
                                } else {
                                    res.json({
                                        error: true,
                                        message: "error on delete category"
                                    })
                                }
                            })
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on deleting Category"})
        }
    })
};

//get one category by Id
//all
//tested
exports.getCategory = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
        if (!err) {
            Category.findOne({_id: req.params.id}, function (err, data) {
                if (!err && data != null) {
                    res.json({error: false, data: data});
                } else {
                    res.json({error: true, massage: "no such category in db!!"});
                }
            })
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    })
};

//get all categories
//all
//tested
exports.getAllCategories = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
        if (!err) {
            Category.find({}, function (err, data) {
                if (!err && data != null) {
                    res.json({error: false, data: data});
                } else {
                    res.json({error: true, massage: "no category in db!!"});
                }
            })
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    })
};

//Update category
//only admins
//tested
exports.updateCategory = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['title', 'descr', 'image'], function (err, errProps) {
                if (!err) {
                 console.log(req.body)
                    Category.findOne({_id: req.params.id}, function (err, cat) {
                        if (!err && cat != null && typeof cat != "undefined") {
                            cat.title = req.body.title;
                            cat.descr = req.body.descr;
                            cat.image = req.body.image;

                            cat.save(function (err, data) {
                                if (!err) {
                                    res.json({error: false, data: data});
                                } else {
                                    res.json({
                                        error: true,
                                        message: "Error on update category!!!"
                                    })
                                }
                            })
                        } else {
                            res.json({error: true, message: "Error on selecting category!!"})
                        }
                    });
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on updating category"})
        }
    })
};



