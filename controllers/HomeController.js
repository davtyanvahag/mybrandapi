var homeItem = require('../models/HomeItem');
var authHelper = require('../helpers/authHelper');
var reqDataHelper = require('../helpers/reqDataHelper');
var fs = require('fs');
var dir = './public/images';
var multer  = require('multer')
var mime = require('mime-types');
var ObjectId = require('mongodb').ObjectID;
var path = require('path');

//create new 
//only admins
//tested
exports.add = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['img', 'category_id'], function (err, errProps) {
                if (!err) {
                    var newItem = new homeItem({
                        category_id: req.body.category_id,
                        img: req.body.img
                    });
                    newItem.save(function (err, data) {
                        if (!err) {
                            res.json({error: false, data: data});
                        } else {
                            res.json({error: true, message: "Error on adding new !"});
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on creating new"})
        }
    })
};

exports.uploadImage = (req, res) => {
  authHelper.checkAdminToken( req, (err, admin) => {
    if (!err) {
      var day = new Date();
      var time = day.getTime();
      var id = new ObjectId();
      var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, dir);
        },
        filename: function (req, file, cb) {
          cb(null, time + '.' + mime.extension(file.mimetype));
        }
      });
      var upload = multer({storage: storage}).single('photo');
      upload(req, res, function (err) {
        if (req.file) {
          if (err) {
            res.json({error: true});
          } else {
            new homeItem({
              _id: id,
              title: '',
              image: time + '.' + mime.extension(req.file.mimetype),
              descr: ''
            }).save( function (err, result) {
              if (!err) {
                res.json({ error: false, id: id });
              } else {
                res.json({error: true});
              }
            });
          }
        } else {
          res.json({error: true});
        }
      });
    }
  });
}

exports.uploadImageEdit = (req, res) => {
  authHelper.checkAdminToken( req, (err, admin) => {
    if (!err) {
      var day = new Date();
      var time = day.getTime();
      var name = '';
      var id = new ObjectId();
      var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, dir);
        },
        filename: function (req, file, cb) {
          cb(null, time + '.' + mime.extension(file.mimetype));
        }
      });
      var upload = multer({storage: storage}).single('photo');
      upload(req, res, function (err) {
        if (req.file) {
          if (err) {
            res.json({error: true});
          } else {
            homeItem.update({_id: req.params.id}, {
              image : time + '.' + mime.extension(req.file.mimetype)
            }, function (err, result) {
              if (!err) {
                res.json({ error: false, id: id });
              } else {
                res.json({error: true});
              }
            });
          }
        } else {
          res.json({error: true});
        }
      });
    }
  });
}

//delete 
//only admins
//tested
exports.delete = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    homeItem.findOne({_id: req.params.id}, function (err, data) {
                        if (!err && data != null) {
                            homeItem.remove({_id: req.params.id}, function (err, data) {
                                if (!err) {
                                    res.json({
                                        error: false,
                                        message: "Deleted successfully"
                                    })
                                } else {
                                    res.json({
                                        error: true,
                                        message: "error on delete "
                                    })
                                }
                            })
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on deleting Price"})
        }
    })
};

//get one by Id
//all
//tested
exports.get = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
        if (!err) {
            var ID = new ObjectId(req.params.id);
            homeItem.findOne({_id: req.params.id}, function (err, data) {
                if (!err && data != null) {
                    res.json({error: false, data: data})
                } else {
                    req.json({error: true, massage: "no prices in db"})
                }
            })
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    })
};

//get  List
//all
//tested
exports.getAll = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
        if (!err) {
            homeItem.find({}, function (err, data) {
                if (!err && data != null) {
                    res.json({error: false, data: data})
                } else {
                    req.json({error: true, massage: "no prices in db"})
                }
            })
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    })
};


exports.updateAndDeleteImage = (req, res) => {
  authHelper.checkAdminToken(req, (err, admin) => {
    if (!err) {
      homeItem.update( {_id : req.body.itemId } ,
        {title : req.body.info.title, descr : req.body.info.descr } ,
        function(err,data) {
          if(!err){
            // path.exists(dir +'/'+ req.body.info.image, function(exists) {
              if (fs.existsSync(dir +'/'+ req.body.info.image)) {
                fs.unlink(dir +'/'+ req.body.info.image);
                res.json({error:false, message:'data upload Succesfuly done'})
              }else{
                  res.json({error:false, message: 'file removed dont exist'})
              }
            // });

          }else{
            res.json({error:true, message: 'something gon wrond please try again', err:err});
          }
        });
    } else {
      res.json({error: true, message: "Error on auth "});
    }
  });
}
//update 
//only admins
//tested
exports.update = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            console.log("REQ : ", req.body);
            homeItem.update({_id: req.body.itemId}, {
                  title : req.body.item.title,
                  descr : req.body.item.descr
            }, function (err, data) {
              console.log("ERR : ", err, "Data : ", data);

              if (!err) {
                res.json({error: false, data: data});
              } else {
                res.json({
                  error: true,
                  message: "Error on update Price!!!"
                })
              }
            });
        } else {
            res.json({error: true, message: "Error on updating Price"})
        }
    })
};



