var jwt = require('jsonwebtoken');
var Admins = require('../models/Admins');
var moment = require('moment');
var authHelper = require('../helpers/authHelper');
var sha256 = require('sha256');
var reqDataHelper = require('../helpers/reqDataHelper');

//registration of new admins
// post data password,  name, username
// only for first time registration
//tested
exports.registration = function (req, res) {
    Admins.findOne({username: req.body.username}, function (err, data) {
        if (err) {
            res.json({error: true, message: "Not Valid,username is already exist"})
        } else {
            var newAdmin = new Admins({
                username: req.body.username,
                email: req.body.email,
                name: req.body.name,
                password: sha256(req.body.password),
                created_at: moment().utc(),
                updated_at: moment().utc()
            });
            newAdmin.save(function (err, data) {
                if (!err) {
                    res.json({error: false, user: data});
                } else {
                    res.json({error: true, message: "Error on registration!", errCode: err});
                }
            })
        }

    })

};
//Admin Log in
// post post data username , password
//tested
exports.logIn = function (req, res) {
    reqDataHelper.validateReqData(req, 'body', ['username', 'password'], function (err, errProps) {
        if (!err) {
            Admins.findOne({username: req.body.username}, function (err, admin) {
                if (!err && admin != null) {
                    if(admin.password == sha256(req.body.password)){
                      const token = jwt.sign({
                        data: {
                          username: admin.username,
                          email: admin.email,
                          id: admin._id
                        }
                      }, global.secret);
                        res.json({error: false, admin: admin, token: token})
                    }
                } else {
                    res.json({error: true, message: "Cant find user. Invalid email or password"});
                }
            });
        } else {
            res.json({error: true, message: "Validation error", errProps: errProps});
        }
    });


};

// registration of new admins
// post data  username,name, password
//tested
exports.addAdmin = function (req, res) {
    console.log(req.body)
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['username', 'name', 'password', 'email'], function (err, errProps) {
                if (!err) {
                    Admins.findOne({username: req.body.username}, function (err, admin) {
                        if (err || admin == null) {
                            var newAdmin = new Admins({
                                username: req.body.username,
                                email: req.body.email,
                                name: req.body.name,
                                password: sha256(req.body.password),
                                created_at: moment().utc(),
                                updated_at: moment().utc(),
                            });
                            newAdmin.save(function (err, newAdmin) {
                                if (!err) {
                                    res.json({error: false, data: newAdmin});
                                } else {
                                    res.json({
                                        error: true,
                                        message: "Error on adding admin!",
                                        errCode: err
                                    });
                                }
                            })
                        } else {
                            res.json({
                                error: true,
                                message: "Username already exist"
                            })
                        }
                    })
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on  creating admin"})
        }
    })
};

//Update admin account
// post data  password, name, username
// tested
exports.updateAccount = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', ['id','name', 'username', 'email'], function (err, errProps) {
                if (!err) {
                    console.log(req.body)
                    Admins.findOne({_id: req.body.id}, function (err, admin) {
                        if (!err && admin != null && typeof admin != "undefined") {
                            // admin.password = req.body.password;
                            admin.name = req.body.name;
                            admin.username = req.body.username;
                            admin.email = req.body.email;
                            admin.updated_at = moment().utc();
                            admin.save(function (err, data) {
                                if (!err) {
                                    res.json({error: false, data: data});
                                    console.log("update", data)
                                } else {
                                    res.json({
                                        error: true,
                                        message: "Error on update account.!"
                                    })
                                }
                            })
                        } else {
                            res.json({error: true, message: "Error on selecting admin!"})
                        }
                    });
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on updating admin account"})
        }
    })
};

//Update admin account
// post data  password, name, username
// tested
exports.updatePassword = function (req, res) {
  authHelper.checkAdminToken(req, function (err, data) {
    if (!err) {
      reqDataHelper.validateReqData(req, 'body', ['id','password'], function (err, errProps) {
        if (!err) {
          Admins.findOne({_id: req.body.id}, function (err, admin) {
            if (!err && admin != null && typeof admin != "undefined") {
              // admin.password = req.body.password;
              admin.password = sha256(req.body.password);
              admin.updated_at = moment().utc();
              admin.save(function (err, data) {
                if (!err) {
                  res.json({error: false, data: data});
                  console.log("update", data)
                } else {
                  res.json({
                    error: true,
                    message: "Error on update account.!"
                  })
                }
              })
            } else {
              res.json({error: true, message: "Error on selecting admin!"})
            }
          });
        } else {
          res.json({error: true, message: "Validation error", errProps: errProps});
        }
      })
    } else {
      res.json({error: true, message: "Error on updating admin account"})
    }
  })
};

// get All admins list
// get request
// tested

exports.getAdminsList = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    Admins.find({},{'name' :1, 'username' : 1, 'email' : 1, '_id' : 1}, function (err, admin) {
                        if (!err && admin != null) {
                            res.json({error: false, data: admin});
                        } else {
                            res.json({error: true, message: "Error, there are no admins in db"});
                        }
                    });
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            });
        } else {
            res.json({error: true, message: "Error on on getting admin list"})
        }
    })
};

//Getting current admin
// checking token with checkAdminToken helper for getting current admin
//tested
exports.getCurrentAdmin = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err && data != null) {
            res.json({error: err, data: data});
        } else {
            res.json({error: err, massage: "invalid token, cant find admin"})
        }
    })
};

exports.getAdmin = function (req, res) {
  Admins.findById(req.params.id,{'name' :1, 'username' : 1, 'email' : 1, '_id' : 1}, function (err, admin) {
    if (!err) {
      if (!err && admin != null) {
        res.json({error: err, data: admin});
      }else{
        res.json({error: err, massage: "no Admin data"})}
    } else {
      res.json({error: err, massage: "find error"})
    }
  })
};

//Delete admins, only the owner of account can delete it
// post data admins id by req params
//tested
exports.deleteAdmin = function (req, res) {
    authHelper.checkAdminToken(req, function (err, data) {
        if (!err) {
            reqDataHelper.validateReqData(req, 'body', [], function (err, errProps) {
                if (!err) {
                    Admins.findById(req.params.id, function (err, admin) {
                        if (!err) {
                            if (!err && admin != null) {
                                if (req.params.id == admin._id) {
                                    Admins.remove({_id: req.params.id}, function (err, data) {
                                        if (!err) {
                                            res.json({
                                                error: false,
                                                message: "Deleted successfully"
                                            })
                                        } else {
                                            res.json({
                                                error: true,
                                                message: "error on delete admin"
                                            })
                                        }
                                    })

                                } else {
                                    res.json({
                                        error: true,
                                        message: "Only owner of account can delete his/her account"
                                    })
                                }
                            } else {
                                res.json({
                                    error: true,
                                    message: "Current current admin is deleted or not found"
                                })
                            }


                        } else {
                            res.json({error: true, message: "Error on selecting admin, no such admin in db!"})
                        }
                    });
                } else {
                    res.json({error: true, message: "Validation error", errProps: errProps});
                }
            })
        } else {
            res.json({error: true, message: "Error on  deleting admin"})
        }
    })
};
