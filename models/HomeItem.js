var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var homeItemSchema = new Schema({
  title: {am: String, en: String},
  descr: {am: String, en: String},
  image: String
});

module.exports = mongoose.model('homeItem', homeItemSchema);