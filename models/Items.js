var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var itemSchema = new Schema({
    title: {am: String, en: String },
    service_id: String,
    image : String
});

module.exports = mongoose.model('items', itemSchema);