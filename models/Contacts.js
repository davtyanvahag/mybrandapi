var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var contactsSchema = new Schema({
  emails: [{name: String}],
  phones: [{phone: String}],
  fb: String,
  insta: String,
  linkedin: String,
  youtube: String,
  addresses: [{am: String, en: String, cords : {lat: Number, long: Number}}]
});

module.exports = mongoose.model('contacts', contactsSchema);