var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var aboutSchema = new Schema({
  descr: { am: String, en: String },
  videolink: [{name: String, link: String}]
});

module.exports = mongoose.model('about', aboutSchema);