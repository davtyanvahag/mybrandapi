var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var partnersSchema = new Schema({
    image: String,
    name: String
});

module.exports = mongoose.model('partners', partnersSchema);